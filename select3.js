;
'use strict';
/**
 * 向jQuery注册组件SearchView
 */
(function($, window) {
	/**
	 * 构建Select对象
	 * @param {Object} _setting	配置对象
	 * @param {Object} _name	组件在表单的名称
	 * @param {Object} _funcDataSource	数据源函数
	 * @param {Object} _funcDecode	反查函数
	 * @param {Object} _funcShowMessage 消息显示函数
	 * @param {Object} _funcClick	单击函数
	 * @param {Object} _funcHover	悬浮函数
	 * @param {Object} _funcMouseout	鼠标移出函数
	 */
	$.fn.select3 = function(	_setting,
								_name,
							   	_funcDataSource,
							   	_funcDecode,
								_funcShowMessage,
							   	_funcClick,
							   	_funcHover,
							   	_funcMouseOut
							  ){
		
		var timeoutId;
		var lastKeydownTimestamp = 0;
		var setting;
		//组件最外层的div
		var self = $(this);
		//组件编号
		var id = self.attr('id');
		//组件名称
		var name = self.data('name');
		//数据源回调函数
		var funcDataSource = _funcDataSource;
		//初始值反差函数
		var funcDecode = _funcDecode;
		//消息提示回调函数
		var funcShowMessage;
		//单击事件回调函数
		var funcClick = _funcClick;
		//鼠标悬浮回调函数
		var funcHover = _funcHover;
		//鼠标移出组件回调函数
		var funcMouseOut = _funcMouseOut;
		//当前页码
		var pageNo = self.data('pageNo');
		//分页页数
		var pageNum = self.data('pageNum');
		//分页大小
		var pageSize = self.data('pageSize');
		//记录总条数
		var total = self.data('total');
		//选中的默认值
		var selectedValue = self.data('value');
		
		if(typeof(selectedValue) == 'number'){
			selectedValue = selectedValue.toString();
		}
		var source = self.data('source');
		
		if(source){
			var source0 = null;
			if(typeof(source) == 'string'){
				try{
					source0 = JSON.parse(source);
				}catch(err){
					throw '<div class="select3-ui" id="'+ id +'" data-value="'+ selectedValue +'" data-source="'+ source +'"></div>  中的source配置错误';
				}
				source = source0;
			}else if(typeof(source) == 'number'){
				throw '<div class="select3-ui" id="'+ id +'" data-value="'+ selectedValue +'" data-source="'+ source +'"></div>  中的source配置错误';
			}
		}else{
			source = {};
		}
		var lastKeyword = "";
		_setting = (!_setting? JSON.parse(self.data('setting')) : _setting);
		setting = $.extend({}, {
				mode:'local',//本地模式
				placeholder:'',//占位符文本
				length: '200px',//界面宽度(单位百分数或者px)
				topOffset: 10,//界面下拉界面与展示框之间的间距(单位px)
				multiple:false,//是否可以多选
				selectParent:false,//是否可以选择父节点
				duplicate:false,//是否可以重复选
				disabled:false,//是否禁用
				readonly:false,//是否只读
				closeWhenSelect:true,//当选择完成后关闭面板，仅在单选有效
				countdownSearch:true,//倒计时搜索
				countdownTime:500,//倒计时时间,单位毫秒
				renderDropdownItemInUiContainer:true,//渲染下拉项UI容器中还是在body内渲染
				pageSize:10,//默认分页大小为10条
				pageNo:0,//默认从第几页开始
				debug:false,//调试模式
				local:{
					searchPlaceholder : '请输入过滤词'
				},
				remote:{
					searchPlaceholder : '请输入搜索关键词'
				}
		}, _setting);
		pageNo = (!pageNo? setting.pageNo : pageNo);
		pageNum = (!pageNum? 0 : pageNum);
		pageSize = (!pageSize? setting.pageSize : pageSize);
		name = (!name? (!_name? id : _name) : name);
		total = (!total? 0 : total);
		
		self.data('name', name);
		self.data('pageNo', pageNo);
		self.data('pageNum', pageNum);
		self.data('pageSize', pageSize);
		self.data('total', total);
		self.data('setting', JSON.stringify(setting));
		
		//如果没有消息函数，则构建一个默认的以alert替代的函数
		if(!_funcShowMessage){
			funcShowMessage = function(title, message){
				alert(message);
			};
		}else{
			funcShowMessage = _funcShowMessage;
		}
		
		if(!_funcDecode){
			funcDecode = function(value){
				return source;
			}
		}
		
		function select3(){
			
		}
		/**
		 * 绑定事件
		 */
		function _bindEvent(){
			var _this = this;
			self.find(".select3-item").click(function() {
				var selectItem = $(this).parent("li");
			});
			$("#"+ id +"__select3-items-box").find(".select3-tree li>.select3-unfold-icon").click(function(e) {
				e.stopPropagation(); //阻止事件冒泡  
				var node = $(this).parent("li");
				if(node.hasClass("select3-tree-unfold")) {
					node.removeClass("select3-tree-unfold");
					node.find(">ul").hide();
				} else {
					node.addClass("select3-tree-unfold");
					node.find(">ul").show();
				}
			});
			$("#"+ id +"__select3-items-box").find(".select3-item").click(function(e) {
				e.stopPropagation(); //阻止事件冒泡
				var node = $(this).parent("li");
				//如果可以多选
				if(setting.multiple){
					//如果可以选择父节点
					if(setting.selectParent){
						//如果可以选择重复节点
						if(setting.duplicate){
							_select(node);
						}else{//如果不可以选择重复节点
							var selecteds = _getSelectedValue();
							if($.inArray(node.data('value'), selecteds) == -1){
								_select(node);
								node.hide();
								node.attr('disabled', 'disabled');
							}
						}
					}else{//如果不能选择父节点，单击项目并且有子节点的仅仅是展开或折叠
						if(node.find("ul>li .select3-item").length > 0){
							if(node.hasClass("select3-tree-unfold")) {//如果已展开，就折叠
								node.removeClass("select3-tree-unfold");
								node.find(">ul").hide();
							} else {//如果处于折叠，就展开
								node.addClass("select3-tree-unfold");
								node.find(">ul").show();
							}
						}else{
							//如果可以选择重复节点
							if(setting.duplicate){
								_select(node);
							}else{//如果不可以选择重复节点
								var selecteds = _getSelectedValue();
								if($.inArray(node.data('value'), selecteds) == -1){
									_select(node);
									node.hide();
									node.attr('disabled', 'disabled');
								}
							}
						}
					}
				}else{//单选
					if(self.data('selected-finish')){
						return;
					}
					//如果可以选择父节点
					if(setting.selectParent){
						_select(node);
						self.data('selected-finish', true);
						//如果为单选将所有选项设置为禁用
						self.find('.select3-option').attr('disabled', 'disabled');
					}else{//如果不能选择父节点，单击项目并且有子节点的仅仅是展开或折叠
						if(node.find("ul>li .select3-item").length > 0){
							if(node.hasClass("select3-tree-unfold")) {//如果已展开，就折叠
								node.removeClass("select3-tree-unfold");
								node.find(">ul").hide();
							} else {//如果处于折叠，就展开
								node.addClass("select3-tree-unfold");
								node.find(">ul").show();
							}
						}else{
							_select(node);
							self.data('selected-finish', true);
							self.find('.select3-option').attr('disabled', 'disabled');
						}
					}
				}
			});
		}
		/**
		 * 选择选项的处理
		 * @param {Object} value
		 * @param {Object} text
		 */
		function _select(node){
			var value = node.data("value");
			var text = node.data("text");
			//随机编号
			var uid = Number(Math.random().toString().substr(3,length) + Date.now()).toString(36);
			var closeBtn = '<i class="select3-selected-icon select3-icon select3-icon-close"></i>';
			self.find(".select3-selected-items").append('<li class="select3-selected-item" data-uid="'+ uid +'" data-value="' + value + '" data-text="' + text + '"><span>' + text + '</span>' + (setting.readonly ? '' :closeBtn ) + '</li>');
			
			$('<input type="hidden" class="select3-selected-input" name="'+ name +'" data-uid="'+ uid +'"  value="' + value + '"></input>').appendTo($("#" + id + "__selected-inputs"));
			//注册选中项单击的事件
			if(!setting.readonly){
				//注册单击删除已选项后如何处理禁用状态
				self.find(".select3-selected-item[data-uid="+ uid +"]").find(".select3-selected-icon").click(function(e){
					e.stopPropagation(); //阻止事件冒泡
					var selectedItem = $(this).parent(".select3-selected-item");
					if(setting.multiple){//如果是多选，则查找选择的备选项进行显示和移出禁用
						if(setting.duplicate){
							
						}else{
							var hiddenSelectItem = $("#"+ id +"__select3-items-box .select3-option[data-value="+selectedItem.data('value')+"]");
							if(hiddenSelectItem.length > 0){
								hiddenSelectItem.removeAttr('disabled');
								hiddenSelectItem.show();
							}
							self.data('selected-finish', false);
						}
					}else{//如果是单选，则移出所有选项的禁用状态，并且显示
						var selectItems = $("#"+ id +"__select3-items-box .select3-option");
						selectItems.removeAttr('disabled');
						selectItems.show();
						self.data('selected-finish', false);
					}
					var _uid = selectedItem.data('uid');
					$("#"+ id +"__selected-inputs").find("input[data-uid="+ _uid +"]").remove();
					selectedItem.remove();
					if(self.find('.select3-selected-item').length > 0){
						_hidePlaceholder();
					}else{
						_showPlaceholder();
					}
				});
			}
			_hidePlaceholder();
			if(setting.multiple){
				if(setting.duplicate){
					
				}else{
					node.hide();
				}
			}else{
				node.hide();
				if(setting.closeWhenSelect){
					_hideDropdownPanel();
				}
			}
		}
		/**
		 * 显示占位符
		 */
		function _showPlaceholder(){
			if(setting.placeholder && $("#"+ id +"__selected-items>.select3-placeholder").length == 0){
				$('<li class="select3-placeholder">' + setting.placeholder + '</li>').appendTo($("#"+ id +"__selected-items"));
			}
		}
		/**
		 * 隐藏占位符
		 */
		function _hidePlaceholder(){
			if(setting.placeholder){
				$("#"+ id +"__selected-items>.select3-placeholder").remove();
			}
		}
		
		function _hideDropdownPanel(target){
			var self = null;
			if(target){
				self = $(target).parent(".select3-dropdown-items");
			}
			$(".select3-dropdown-items").each(function(index, ele){
				var dropdownItems = $(ele);
				//如果当前进行点击操作的元素是 面板，则跳过该面板的隐藏
				if(self && self.attr("id") == dropdownItems.attr("id")){
					return;
				}
				dropdownItems.hide();
				dropdownItems.find(".select3-search-input").val('');
			});
		}
		
		/**
		 * 渲染UI界面
		 * @param {Object} data 数据
		 */
		function _renderUI(data){
			var selectedValues = self.data('selectedValues');
			selectedValues = selectedValues ? selectedValues : _getSelectedValue();
			selectedValues = ((typeof(selectedValues) == 'string')? JSON.parse(selectedValues): selectedValues);
			//JSON数据存在nodes节点，且存在至少一个结果进行渲染
			return _renderNodes(data.nodes, 0, selectedValues);
		}
		
		/**
		 * 渲染节点数据
		 * @param {Object} nodes 节点数据
		 * @param {Object} deep 深度
		 * @param {Object} selectedValues 已选中的值
		 */
		function _renderNodes(nodes, deep, selectedValues){
			var root = $('<ul class="select3-tree" style="' + (deep != 0 ? 'padding-left: 16px;display: none;' : '') + '"></ul>');
			for(var index in nodes) {
				var node = nodes[index];
				var element = _renderNode(node, selectedValues);
				//存在子节点
				if(node.nodes) {
					element.append('<i class="select3-unfold-icon select3-icon select3-icon-enter"></i>');
					var tree = _renderNodes(node.nodes, ++deep, selectedValues);
					tree.appendTo(element);
				}
				element.appendTo(root);
			}
			return root;
		}
		/**
		 * 渲染节点
		 * @param {Object} node 节点数据
		 * @param {Object} selectedValues 已选中的值
		 */
		function _renderNode(node, selectedValues){
			var icon = node.icon ? node.icon : '';
			var value = node.value;
			var text = node.text;
			var selected = node.selected;
			var disabled = false;
			if(setting.multiple){//如果是多选
				for(var idx in selectedValues){
				var selectedValue = selectedValues[idx];
					if(selectedValue == value){
						selected = true;
						disabled = true;
					}
				}
			}else{//如果是单选
				if(self.data('selected-finish')){
					disabled = true;
				}
			}
			var element = $('<li class="select3-option" data-icon="' + icon + '" data-value="' + value + '" data-text="' + text + '" '+ (disabled ? 'disabled="disabled" ': '') + (selected ? 'style="display:none;"' : '') +'><i class="select3-item-icon select3-icon ' + icon + '"></i><span  class="select3-item">' + text + '</span></li>');
			if(node.selected){
				_select(node);
			}
			return element;
		}
		
		/**
		 * 本地搜索
		 * @param {Object} keyword 关键词
		 */
		function _localSearch(keyword){
			var matchCount = 0;
			$("#"+ id +"__select3-items-box").find(".select3-option").each(function(index, ele){
				var option = $(ele);
				var item = option.find(">.select3-item");
				var text = option.data('text');
				var value = option.data('value');
				var pos = text.indexOf(keyword);
				//如果关键词没有，则全部恢复
				if(!keyword || keyword.length == 0){
					item.html(text);
					if(!option.attr('disabled')){
						option.show();
						option.parents('.select3-option').show();
					}
					matchCount++;
					return;
				}
				if(pos>-1){
					item.html(text.slice(0,pos)
						+ '<span class="select3-search-result">'
						+ keyword
						+ '</span>'
						+ text.slice(pos + keyword.length)
					);
					if(!option.attr('disabled')){
						option.show();
						option.parents('.select3-option').show();
					}
					matchCount++;
				}else{
					option.hide();
				}
			});
			//没有匹配的结果，显示提示信息
			var remindBox = $("#"+ id +"__select3-remind-box");
			if(matchCount == 0){
				remindBox.html('无匹配的选项！');
				remindBox.show();
			}else{
				remindBox.html('');
				remindBox.hide();
			}
		};
		/**
		 * 远程搜索
		 * @param {Object} keyword 关键字
		 */
		function _remoteSearch(keyword, _pageNo){
			if(setting.debug){
				console.log(new Date().getTime() + ":正在触发搜索");
			}
			if(!funcDataSource){
				funcShowMessage('错误','未提供数据源函数!');
				return;
			}
			if(lastKeyword != keyword){//如果搜索关键词不一样，则重置为1
				pageNo = 1;
			}else{
				pageNo = _pageNo;
			}
			
			var data = funcDataSource(instance, keyword, pageSize, pageNo);
			//如果无数据返回，可能是数据源回调接口已在内部执行设置数据源函数
			if(data != undefined && data != null){
				//发送获取远程数据源
				_setDataSource(data);
			}
			//每次搜索完保存上一次的搜索结果
			lastKeyword = keyword;
		}
		/**
		 * 刷新
		 */
		function _refresh(){
			var keyword = $("#"+ id + "__select3-search-input").val();
			if(setting.mode == 'local'){
				_localSearch(keyword);	
			}else{
				_remoteSearch(keyword, 1);
			}
		}
		/**
		 * 绘制组件
		 */
		function _draw(){
			var _this = this;
			//清空当前组件最外层以内的所有元素
			self.empty();
			var pagination = '<!--分页区-->'
				+'<div class="select3-pagination" id="'+ id +'__pagination">'
					+'<i class="select3-pagination-button select3-icon select3-icon-skip-previous"></i>'
					+'<i class="select3-pagination-button select3-icon select3-icon-chevron-left"></i>'
					+'<span class="select3-pagination-text" id="'+ id +'__pagination-pageNo">'+ pageNo +'</span><span class="select3-pagination-text">/</span><span class="select3-pagination-text"  id="'+ id +'__pagination-pageNum">'+ pageNum +'</span>'
					+'<i class="select3-pagination-button select3-icon select3-icon-chevron-right"></i>'
					+'<i class="select3-pagination-button select3-icon select3-icon-skip-next"></i>'
				+'</div>';
			var placeholder = (setting.placeholder ? '<li class="select3-placeholder">'+ setting.placeholder +'</li>' : '');
			var refreshBtn = '<span class="select3-refresh-btn select3-icon select3-icon-refresh" id="'+ id +'__select3-refresh-btn"></span>';
			self.append('<div class="select3-selected-bar" id="'+ id +'__select3-selected-bar">'
							+'<ul class="select3-selected-items" id="'+ id +'__selected-items">'
							+ placeholder
							+'</ul>'
							+'<div id="'+ id +'__selected-inputs" style="display:none;"></div>'
							+ (setting.readonly ? '' : refreshBtn)
						+'</div>');
			var x = $("#"+ id).offset().left;
			var y = $("#"+ id).height() + $("#"+ id).offset().top;
			var dropdownParent = null;
			var posStyle = null;
			if(setting.renderDropdownItemInUiContainer){
				dropdownParent = self;
			}else{
				dropdownParent = $("html");
				posStyle = "left:" + x + "px;top:" + y + "px;";
			}
			dropdownParent.append('<div class="select3-dropdown-items" id="'+ id +'__select3-dropdown-items" style="width: calc('+ self.width() +'px - 2px); display: none;'+ (setting.renderDropdownItemInUiContainer ? '' : posStyle) +'">'
							+'<div class="select3-search-bar">'
								+'<input class="select3-search-input" id="'+ id +'__select3-search-input" placeholder="'+ (setting.mode == 'local' ? setting.local.searchPlaceholder:setting.remote.searchPlaceholder) +'">'
								+'<span class="select3-search-btn select3-icon '+ (setting.mode == 'local' ? 'select3-icon-filter' : 'select3-icon-search') +'" id="'+ id +'__select3-search-btn"></span>'
							+'</div>'
							+'<div class="select3-items-box" id="'+ id +'__select3-items-box">'
							+'</div>'
							+ (setting.mode == 'remote' ? pagination : '')
							+'<!--信息提示区-->'
							+'<div class="select3-remind-box" id="'+ id +'__select3-remind-box" style="display: none;">'
							+'	无匹配的选项！'
							+'</div>'
						+'</div>');
			//注册鼠标在组件外单击，隐藏组件下拉框
			$(document).on("click", function(e){
				_hideDropdownPanel(e.target);
			});
			
			$("#" + id + "__select3-dropdown-items").click(function(e){
				e.stopPropagation();
			});
			
			if(!setting.readonly){
				$("#" + id + "__select3-selected-bar").click(function(e) {
					$("#" + id + "__select3-dropdown-items").toggle();
					//阻止事件冒泡
					e.stopPropagation();
				});
			}

			//注册单击搜索按钮，触发数据源函数
			$("#"+ id + "__select3-search-btn").click(function(e){
				e.stopPropagation(); //阻止事件冒泡
				_refresh();
			});
			//注册搜索框按回车，触发数据源函数
			$("#"+ id + "__select3-search-input").keyup(function(e){
				//如果不是回车，则不进行事件处理
				e.stopPropagation(); //阻止事件冒泡
				if(setting.countdownSearch){
					if(setting.debug){
						console.log(new Date().getTime() + ":new Date().getTime() - lastKeydownTimestamp < setting.countdownTime:" + new Date().getTime() - lastKeydownTimestamp < setting.countdownTime);
					}
					if(new Date().getTime() - lastKeydownTimestamp < setting.countdownTime){
						if(timeoutId > 0){
							if(setting.debug){
								console.log(new Date().getTime() + ":撤销倒计时");
							}
							clearTimeout(timeoutId);
						}
					}
					if(setting.debug){
						console.log(new Date().getTime() + ":setting.countdownTime:" + setting.countdownTime);
					}
					timeoutId = setTimeout(function(){
						_refresh();
					}, setting.countdownTime);
					lastKeydownTimestamp = new Date().getTime();
				}else{
					if(13 == event.keyCode){
						var remindBox = $("#"+ id +"__select3-remind-box");
						remindBox.html('请单击右侧搜索按钮进行操作！');
						remindBox.show();
					}
					
				}
			});
			
			$("#"+ id +"__select3-refresh-btn").click(function(e){
				e.stopPropagation(); //阻止事件冒泡
				self.data('selected-finish', false);
				_hideDropdownPanel();
				_cleanSelecteds();
				_showPlaceholder();
				var remindBox = $("#"+ id +"__remind-box");
				remindBox.html('');
				remindBox.hide();
				if(setting.mode == 'remote'){
					_setDataSource({
						pageNo:0,
						pageNum:0,
						total:0
					});
				}
			});
			//如果是远程模式则注册事件
			if(setting.mode == 'remote'){
				$("#"+ id +"__pagination .select3-icon-chevron-left").click(function(e){
					e.stopPropagation(); //阻止事件冒泡
					var _prePageNo = _getPrePageNo();
					if(_prePageNo == pageNo){
						return;
					}
					var keyword = $("#"+ id + "__select3-search-input").val();
					_remoteSearch(keyword, _prePageNo);
				});
				
				$("#"+ id +"__pagination .select3-icon-skip-previous").click(function(e){
					e.stopPropagation(); //阻止事件冒泡
					var keyword = $("#"+ id + "__select3-search-input").val();
					_remoteSearch(keyword, 1);
				});
				
				$("#"+ id +"__pagination .select3-icon-chevron-right").click(function(e){
					e.stopPropagation(); //阻止事件冒泡
					var _nextPageNo = _getNextPageNo();
					if(_nextPageNo == pageNo){
						return;
					}
					var keyword = $("#"+ id + "__select3-search-input").val();
					_remoteSearch(keyword, _getNextPageNo());
				});
				
				$("#"+ id +"__pagination .select3-icon-skip-next").click(function(e){
					e.stopPropagation(); //阻止事件冒泡
					var _lastPageNo = _getLastPageNo();
					if(_lastPageNo == pageNo){
						return;
					}
					var keyword = $("#"+ id + "__select3-search-input").val();
					_remoteSearch(keyword, _lastPageNo);
				});
			}
			//通过反查函数返回节点数据
			var dataSource = funcDecode(instance, selectedValue);
			if(dataSource){
				_setSelectedValue(selectedValue, dataSource);
			}
		}
		/**
		 * 获取最后一页页码
		 */
		function _getLastPageNo(){
			var _pageNo = self.data("pageNo");
			var _total = self.data("total");
			if(_total == 0){
				_pageNo = 0;
			}else if(Math.floor((total + pageSize -1 ) / pageSize) > pageNo){
				_pageNo = Math.floor((total + pageSize -1 ) / pageSize);
			}
			return _pageNo;
		}
		/**
		 * 获取前一页的页码
		 */
		function _getPrePageNo(){
			var _pageNo = self.data("pageNo");
			
			_pageNo = Math.max(_pageNo > 0 ? _pageNo -1: 1, 1);
			return _pageNo;
		}
		/**
		 * 获取下一页的页码
		 */
		function _getNextPageNo(){
			var _pageNo = Math.min(pageNo + 1, _getLastPageNo());
			return _pageNo;
		}

		/**
		 * 获取当前页页码
		 */
		function _getPageNo(){
			var _lastPageNo = _getLastPageNo();
			if(pageNo< _lastPageNo){
				return pageNo;
			}else{
				return _lastPageNo;
			}
		}
		/**
		 * 设置总共页数
		 * @param {Object} _pageNum 页数
		 */
		function _setPageNum(_pageNum){
			if(!_pageNum == undefined || _pageNum == null){
				return;
			}
			$("#"+ id +"__pagination-pageNum").html(_pageNum);
			pageNum = _pageNum;
			self.data('pageNum', _pageNum);
		}
		/**
		 * 设置当前页页码
		 * @param {Object} _pageNo
		 */
		function _setPageNo(_pageNo){
			if(!_pageNo == undefined || _pageNo == null){
				return;
			}
			$("#"+ id +"__pagination-pageNo").html(_pageNo);
			pageNo = _pageNo;
			self.data('pageNo', pageNo);
		}
		
		function _setTotal(_total){
			if(_total == undefined || _total == null){
				return;
			}
			total = _total;
			self.data('total', total);
		}
		/**
		 * 设置数据源
		 * @param {Object} data 数据JSON对象格式或者字符串格式
		 */
		function _setDataSource(data){
			if(data == undefined || data == null){
				data = {};
			}
			if(typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			_cleanOptions();
			_setTotal(data['total']);
			_setPageNo(data['pageNo']);
			_setPageNum(data['pageNum'])
			if(data.nodes && data.nodes.length > 0){
				var dom = _renderUI(data);
				dom.appendTo($("#"+ id +"__select3-items-box"));
				var ops = _doOptions(data.nodes, null);
				self.data("dataSource", data);
				self.data("options", JSON.stringify(ops));
				if(setting.mode == 'remote'){//如果是远程模式，且有结果返回时
					//有匹配的结果，隐藏提示信息
					var remindBox = $("#"+ id +"__select3-remind-box");
					remindBox.html('');
					remindBox.hide();
				}
			}else{
				self.data("dataSource", data);
				self.data("options", JSON.stringify([]));
				if(setting.mode == 'remote'){//如果是远程模式，且没有结果返回时
					//没有匹配的结果，显示提示信息
					var remindBox = $("#"+ id +"__select3-remind-box");
					remindBox.html('换个关键字试试！');
					remindBox.show();
				}
				pageNo = 0;
			}
			_bindEvent();
		}
		/**
		 * 获取对象格式的数据源JSON
		 */
		function _getDataSource(){
			var json = self.data("dataSource");
			if(typeof(json) == 'string') {
				json = JSON.parse(json);
			}
			return json;
		}
		/**
		 * 设置选中的选项
		 * @param {Object} selectedValueString 选中值字符串形式或者数组
		 * @param {Object} dataSource 选中时使用的数据源
		 */
		function _setSelectedValue(selectedValueString, dataSource){
			//如果提供数据源，说明为第一次选中默认值的情况
			if(dataSource){
				_setDataSource(dataSource);
			}
			if(!selectedValueString){
				selectedValueString = selectedValue;
				if(typeof(selectedValueString) == 'string') {
					selectedValueString = selectedValueString.split(",");
				}else if(typeof(selectedValueString) == 'number'){
					selectedValueString = [selectedValueString];
				}
			}else{
				if(typeof(selectedValueString) == 'string') {
					selectedValueString = selectedValueString.split(",");
				}else if(typeof(selectedValueString) == 'number'){
					selectedValueString = [selectedValueString];
				}
				selectedValue = selectedValueString;
			}
			var selects = [];
			for (var index in selectedValueString) {
				var value = selectedValueString[index];
				if($.inArray(value, selects)==-1){//不允许重复
					selects.push(value);//没有重复的时候，直接加入
				}else{
					if(setting.duplicate){//如果允许重复
						selects.push(value);
					}
				}
			}
			var options = _getOptions();
			for (var i = 0; i < options.length; i++) {
				var option = options[i]
				//检查选线是否在需要设置的选项值
				if($.inArray(option['value'], selects)> -1){
					//如果允许重复，则需要按照选中值进行遍历
					if(setting.duplicate){
						for(var index in selects){
							var val = selects[index];
							if(option['value'] == val){
								var nodes = $("#"+ id + "__select3-dropdown-items").find('.select3-option[data-value="'+ val +'"]');
								if(nodes.length <= 0){
									throw "无效编码" + val;
								}
								_select($(node[0]));
							}
						}
					}else{//不允许重复的多选
						var val = option['value'];
						var nodes = $("#"+ id + "__select3-dropdown-items").find('.select3-option[data-value="'+ val +'"]');
						if(nodes.length <= 0){
							throw "无效编码" + val;
						}
						_select($(nodes[0]));
					}
				}
			}
			self.data("selected-value", selects);
			
			if(setting.multiple){
				if(setting.duplicate){//如果可以重复选择，不处理
					
				}else{//不是重复选择，则需要将当前选中节点处理为不可选
					for (var index in selectedValueString) {
						var val = selectedValueString[index];
						$("#"+ id + "__select3-dropdown-items").find('.select3-option[data-value="'+ val +'"]').attr('disabled', 'disabled');
					}
				}
			}else{
				//如果为单选将所有选项设置为禁用
				self.find('.select3-option').attr('disabled', 'disabled');
			}
		}
		/**
		 * 获取已选择的编码值数组
		 */
		function _getSelectedValue(){
			if(setting.multiple){
				var values = [];
				self.find(".select3-selected-item").each(function(index, item) {
					values.push($(item).data("value"));
				});
				if(setting.debug){
					console.log(new Date().getTime() + ":_getSelectedValue() values:" + JSON.stringify(values));
				}
				return values;
			}else{
				var value = null;
				var selects = self.find(".select3-selected-item");
				if(selects.length > 0){
					value = $(selects[0]).data("value");
				}
				if(setting.debug){
					console.log(new Date().getTime() + ":_getSelectedValue() value:" + value);
				}
				return value;
			}
			
		}
		/**
		 * 获取已选择的文字数组
		 */
		function _getSelectedText(){
			if(setting.multiple){
				var selectedText = [];
				self.find(".select3-selected-item").each(function(index, item) {
					selectedText.push($(item).data("text"));
				});
				if(setting.debug){
					console.log(new Date().getTime() + ":_getSelectedText() texts:" + val);
				}
				return selectedText;
			}else{
				var text = null;
				var selects = self.find(".select3-selected-item");
				if(selects.length > 0){
					text = $(selects[0]).data("text");
				}
				if(setting.debug){
					console.log(new Date().getTime() + ":_getSelectedText() text:" + text);
				}
				return text;
			}
		}
		
		/**
		 * 清空备选选项
		 */
		function _cleanOptions(){
			$("#"+ id +"__select3-items-box").empty();
			self.data("dataSource", '{}');
		}
		/**
		 * 清空已选选项
		 */
		function _cleanSelecteds(){
			self.find(".select3-selected-icon").each(function(index, item) {
				var selectedItem = $(item).parent(".select3-selected-item");
				//对于隐藏的备选项进行恢复为可选
				var selectItems = $("#"+ id +"__select3-items-box .select3-option[data-value="+selectedItem.data('value')+"]");
				selectItems.removeAttr('disabled');
				selectItems.show();
				selectedItem.remove();
			});
			self.find("#"+id+"__selected-inputs").empty();
			self.data('selected-finish', false);
			_showPlaceholder();
		}
		
		/**
		 * 处理数据源中的节点数据为选项数组
		 * @param {Object} nodes 数据源节点
		 * @param {Object} options 选项数组
		 */
		function _doOptions(nodes, options) {
			if(!options) {
				options = [];
			}
			for(var index in nodes) {
				var node = nodes[index];
				var option = {};
				option['value'] = node.value;
				option['text'] = node.text;
				options.push(option);
				if(node.nodes) {
					_doOptions(node.nodes, options);
				}
			}
			return options;
		}
		
		function _getOptions(){
			var json = self.data("options");
			if(!json){
				return [];
			}
			if(typeof(json) == 'string') {
				json = JSON.parse(json);
			}
			return json;
		}
		
		select3.prototype = {
			constructor: select3,
			/**
			 * 选中给定代码值和文本的选项
			 * @param {Object} value 代码值
			 * @param {Object} text 文本
			 */
			select: function(value, text){
				var nodes = $("#"+ id + "__select3-dropdown-items").find('.select3-option[data-value="'+ value +'"]');
				if(nodes.length <= 0){
					throw "无效编码值" + value;
				}
				return _select($(nodes[0]));
			},
			/**
			 * 绘制组件
			 */
			draw: function(){
				_draw();
				return this;
			},
			/**
			 * 在一个给定的组件ID以JSON对象设置数据源
			 * @param {Object} data 形如{nodes:[{text:'测试项1', value:123},{text:'测试项2', value:456}], pageSize:20, pageNo:1, total:20}
			 */
			setDataSource: function(data) {
				_setDataSource(data);
				return this;
			},
			/**
			 * 获取数据源，返回得是一个JSON对象
			 */
			getDataSource: function() {
				return _getDataSource();
			},
			/**
			 * 在一个给定的组件ID设置选中的值，设置值可为整数，字符串，整数数组或字符串数组
			 * @param {Object} value 选中的编码值，例如123或者123,234,345
			 * @param {Object} dataSource 选中项使用的数据源，用于在加载第一次时选中默认值
			 */
			setSelectedValue: function(data, dataSource) {
				_setSelectedValue(data, dataSource);
				return this;
			},
			/**
			 * 在一个给定的组件ID获取选中的值，返回结果为选中的编码值数组
			 */
			getSelectedValue: function() {
				var val = _getSelectedValue();
				if(setting.debug){
					console.log(new Date().getTime() + ":getSelectedValue() val:" + JSON.stringify(val));
				}
				return val;
			},
			/**
			 * 在一个给定的组件ID获取选中的展示值，返回结果为选中的展示值数组
			 */
			getSelectedText: function() {
				var text = _getSelectedText();
				if(setting.debug){
					console.log(new Date().getTime() + ":getSelectedText() val:" + JSON.stringify(text));
				}
				return text;
			},
			/**
			 * 清除已选择的选项
			 */
			cleanSelecteds: function(){
				_cleanSelecteds();
				return this;
			},
			/**
			 * 获取选项数组
			 */
			getOptions: function() {
				return _getOptions();
			},
			/**
			 * 清除备选项
			 */
			cleanOptions: function() {
				_cleanOptions();
				return this;
			}
		};
		
		var instance = new select3(_funcShowMessage,
							   _funcDataSource,
							   _funcClick,
							   _funcHover,
							   _funcMouseOut,
							   _setting);
		var drawed = self.data('drawed');
		if(!drawed){
			//绘制组件
			instance.draw();
			self.data('drawed', true);
		}
		return instance;
	};
})(jQuery, window);
